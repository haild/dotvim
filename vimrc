" vim-plug https://github.com/junegunn/vim-plug {{{
call plug#begin()
Plug 'tpope/vim-sensible'

" completion
Plug 'ervandew/supertab'

" IDE
Plug 'scrooloose/syntastic'
Plug 'davidhalter/jedi-vim'
Plug 'hynek/vim-python-pep8-indent'
Plug 'nvie/vim-flake8'
Plug 'heavenshell/vim-pydocstring'
Plug 'joonty/vim-phpqa'
Plug 'dag/vim2hs'
Plug 'lervag/vimtex'
Plug 'janko-m/vim-test'

" UI
Plug 'bling/vim-airline'
Plug 'will133/vim-dirdiff'
Plug 'chrisbra/vim-diff-enhanced'
Plug 'airblade/vim-gitgutter'

" Productivity
Plug 'tpope/vim-fugitive'
Plug 'junegunn/vim-easy-align'
Plug 'fisadev/vim-isort'

" Navigation
Plug 'kien/ctrlp.vim'
Plug 'scrooloose/nerdtree'
Plug 'jlanzarotta/bufexplorer'
Plug 'dkprice/vim-easygrep'

" syntax
Plug 'dag/vim-fish'
Plug 'ekalinin/Dockerfile.vim'
Plug 'mfukar/robotframework-vim'
Plug 'vim-scripts/nginx.vim'
Plug 'lifepillar/pgsql.vim'
Plug 'othree/html5.vim'
Plug 'Glench/Vim-Jinja2-Syntax'
Plug 'chase/vim-ansible-yaml'

" colorscheme
Plug 'morhetz/gruvbox'
Plug 'crusoexia/vim-monokai'
Plug 'lifepillar/vim-solarized8'
Plug 'skielbasa/vim-material-monokai'
call plug#end()
" }}}

" Performance
set shell=bash
set noswapfile

set lazyredraw
set modeline!

" Editing
set shiftwidth=4
set tabstop=4
set softtabstop=4
set smarttab
set expandtab
noremap <Leader>p "_ddP

" UI
"set switchbuf+=usetab,newtab
set relativenumber
set number
set laststatus=2

set background=dark

if has('termguicolors')
    set termguicolors
    set t_ZH=[3m
    set t_ZR=[23m
    let g:materialmonokai_italic=1
    let g:airline_theme='materialmonokai'
    colorscheme material-monokai
else
    if has("gui_running")
        colorscheme solarized
    else
        set t_Co=256
        set t_ZH=[3m
        set t_ZR=[23m
        let g:monokai_italic = 1
        colorscheme monokai
    endif
endif

" Custom filetype configuration
au BufNewFile,BufRead COMMIT_EDITMSG setlocal spell
au BufNewFile,BufRead .arc* setlocal ft=json
au BufNewFile,BufRead Vagrantfile* setlocal ft=ruby
au BufNewFile,BufRead vim* setlocal foldmethod=marker
au BufNewFile,BufRead *.sh setlocal foldmethod=marker
" Press K for help when editing vim files
au FileType vim setlocal keywordprg=:help

" Custom key mapping
" NERDTree
map <F9> :NERDTreeToggle<CR>

" Write with sudo when vim started with unprevilege user
cmap w!! w !sudo tee > /dev/null %

" Tab
nnoremap <Space> :bnext<CR>
set hidden

let g:EasyGrepRecursive=1
let g:EasyGrepMode=2
let g:EasyGrepJumpToMatch=0

let g:airline#extensions#tabline#enabled=1
let g:airline_powerline_fonts=1
let g:airline#extensions#tabline#fnamemod = ':s?.virtualenvs/.*/lib/python2.7/site-packages?venv?:~:.'

let g:jedi#show_call_signatures = "2"

set noshowmode
set mouse=nv
set clipboard=unnamed

" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)

" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)

let g:DirDiffAddArgs = "-w"

let g:phpqa_codesniffer_args = "--standard=PSR2"
" Stop the location list opening automatically
let g:phpqa_open_loc = 0
" Don't run messdetector on save (default = 1)
let g:phpqa_messdetector_autorun = 0

let g:vimtex_latexmk_options = '-pdf -file-line-error -synctex=1 -interaction=nonstopmode -xelatex'

let g:sql_type_default = 'pgsql'

