Installation::

    git clone git@bitbucket.org:haild/dotvim.git ~/.vim

Create symlinks::

    ln -s ~/.vim/vimrc ~/.vimrc

Switch to the ``~/.vim`` directory, and install Vundle submodule::

    cd ~/.vim
    git submodule init
    git submodule update

Install Vim plugins::

    vim +PluginInstall +qa
